FROM node:latest

WORKDIR /app

COPY . ./


ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./

RUN npm install --silent

EXPOSE 4000

CMD ["node" , "server.js"]
